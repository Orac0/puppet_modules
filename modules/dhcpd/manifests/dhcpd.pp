# Usage: Do not include this class; instead include one or more subclasses,
# and include dhcpd::finalize last.
# Each dhcp server "domain" is defined in a subclass. A domain is just a scope
# that we provide dhcp service for. A dhcp server can run multiple domains if
# desired although they might collide on some options so check first.
# Note that you must include at least one domain (subclass), or the dhcp server
# will fail at startup and therefore make the puppet run fail.
class dhcpd::dhcpd {
        distro::check { $module_name: supported => ['squeeze', 'wheezy'] }

        $dhcpd_confdir = '/etc/dhcp'

        package {
                "isc-dhcp-server":
                        ensure => "installed";
        }

        service {
                "isc-dhcp-server":
                        enable => true,
                        ensure => running,
                        hasstatus => true,
                        hasrestart => true,
                        require => Package["isc-dhcp-server"];
        }

        munin::plugin {
                "dhcpd_stats":
                        source => "puppet:///modules/dhcpd/munin-dhcpd_stats",
                        require => Package["isc-dhcp-server"];
        }

        file {
                "${dhcpd_confdir}":
                        owner => puppet_provisioning,
                        group => adm,
                        mode => 2775;
                "${dhcpd_confdir}/dhcpd.common":
                        source => 'puppet:///modules/dhcpd/dhcpd.common';
                "${dhcpd_confdir}/classes.conf":
                        source => 'puppet:///modules/dhcpd/classes.conf',
                        owner => puppet_provisioning,
                        group => adm,
                        mode => 664,
                        replace => false;
                '/etc/logrotate.d/dhcpd':
                        source => "puppet:///modules/dhcpd/logrotate-dhcpd";
# FIXME: Usage/used? Fails.
#               "/usr/bin/dhcpd_worst_offenders":
#                       mode => "0755",
#                       source => "puppet:///modules/dhcpd/dhcpd_worst_offenders.rb",
#                       require => Package["isc-dhcp-server"];
        }

        include rsyslogd::dhcpd

        include users
        realize (
                Localuser["provisioning"],
        )

}