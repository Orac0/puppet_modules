class userconfig::userconfig {

        # Copies all files recursively into user's homedir.
        define sync(
                $homedir
        ) {
                file {
                  "${homedir}":
                        require => User["$name"],
                        source => ["puppet:///modules/userconfig/${name}", "puppet:///modules/common/empty.dir"],
                        owner => $name,
                        group => $name,
                        # Becomes 700 on dirs.
                        mode => 600,
                        # Do not touch files that only exists on the client host.
                        recurse => remote,
                        ignore => ".svn";
                }
        }

}
